<?php

namespace AppBundle\Twig;

class FilterVista extends \Twig_Extension {
  
    public function getFilters() {
        return [
            new \Twig_SimpleFilter("addText",[$this,'addText'])  
        ];
    }
    
    public function addText($string){
        return $string." TEXTO CONCATENADO";
    }

    public function getName() {
        return "filter_vista";
    }
}

