<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Curso;
use AppBundle\Form\CursoType;
use Symfony\Component\Validator\Constraints as Assert;

class PruebasController extends Controller
{
 
    public function indexAction(Request $request,$name,$page)
    {
        
      //  return $this->redirect($this->container->get("router")->getContext()->getbaseUrl()
      //                  ."/hello-world?hola=true");
     
     //     return $this->redirect($request->getbaseUrl()."/hello-world?hola=true");
     
   //  var_dump($request->query->get("hola"));
   //  var_dump($request->get("hola-post"));
   //  die();
     
        // replace this example code with whatever you need
//        $productos=[
//            "producto"=>[
//                ["nombre"=>"consola 1","precio"=>2],
//                ["nombre"=>"consola 2","precio"=>5],
//                ["nombre"=>"consola 3","precio"=>7],
//                ["nombre"=>"consola 4","precio"=>10],
//            ]
//            ]; 
        $productos=array(
            array("producto"=>"Consola 1","precio"=>2),
            array("producto"=>"Consola 2","precio"=>7),
            array("producto"=>"Consola 3","precio"=>8),
            array("producto"=>"Consola 4","precio"=>9),
            array("producto"=>"Consola 5","precio"=>10)
        );
        
        $fruta=array(
            "manzana"=>"golden",
            "pera"=>"rica"
        );
        
        return $this->render('AppBundle:pruebas:index.html.twig', array(
            'texto' => "Nombre: ".$name." - Página: ".$page,
            'productos'=>$productos,
            'fruta'=>$fruta
        ));
    }
    
    public function createAction(){
        
        $curso=new Curso();
        $curso->setTitulo("Curso de Symfony3 Victor Robles");
        $curso->setDescripcion("Curso completo de Symfony3");
        $curso->setPrecio(80);
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($curso);
        $flush=$em->flush();
        
        if($flush!=null){
            echo "El curso no se ha creado bien!!";
        } else {
            echo "El curso se ha creado correctamente";
        }
        
        die();
    }
    
    public function readAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $cursos_repo=$em->getRepository("AppBundle:Curso");
        $cursos=$cursos_repo->findAll();
        
        $curso_veinte=$cursos_repo->findOneByPrecio(25);
        
       // foreach ($curso_veinte as $curso){
        echo $curso_veinte->getTitulo();
       //}
       
//        foreach ($cursos as $curso){
//            echo $curso->getTitulo()."<br/>";
//            echo $curso->getDescripcion()."<br/>";
//            echo $curso->getPrecio()."<hr/>";
//            
//        }
        
        die();
    }
    
    public function updateAction($id,$titulo,$descripcion,$precio){
          $em=$this->getDoctrine()->getEntityManager();
          $cursos_repo=$em->getRepository("AppBundle:Curso");
       
          $curso=$cursos_repo->find($id);
          $curso->setTitulo($titulo);
          $curso->setDescripcion($descripcion);
          $curso->setPrecio($precio);
          
          $em->persist($curso);
          
          $flush=$em->flush();
          
          if($flush!=null){
              echo "El curso no se ha actulizado!!!";
          } else{
              echo "El curso se actualizó correctamente";
          }
          
          die();
    }
    
    public function deleteAction($id){
         $em=$this->getDoctrine()->getEntityManager();
         $cursos_repo=$em->getRepository("AppBundle:Curso");
         
         $curso=$cursos_repo->find($id);
         $em->remove($curso);
         
         $flush=$em->flush();
         
         if($flush!=null){
             echo "El curso no se a podido eliminar";
         } else {
             echo "El curso se eliminó";
         }
         
         die();
       
    }
    
    public function nativeSqlAction(){
         $em=$this->getDoctrine()->getEntityManager();
         $db=$em->getConnection();
         $query="SELECT * FROM cursos";
         $stmt=$db->prepare($query);
         $params=array();
         $stmt->execute($params);
         
         $cursos=$stmt->fetchAll();
         
         foreach ($cursos as $curso){
             
             echo $curso["id"]."<br>";
             echo $curso["titulo"]."<br>";
             echo $curso["descripcion"]."<br>";
             echo $curso["precio"]."<br>";
             
         }
         
         die();
         
    }
    
    public function dqlAction(){
          $em=$this->getDoctrine()->getEntityManager();
          
          $query=$em->createQuery(" 
                  SELECT c FROM AppBundle:Curso c
                  WHERE c.precio > :precio
                  ")->setParameter("precio","24");
          
          $cursos=$query->getResult();
          
            foreach ($cursos as $curso){
             
             echo $curso->getId()."<br>";
             echo $curso->getTitulo()."<br>";
             echo $curso->getDescripcion()."<br>";
             echo $curso->getPrecio()."<br>";
             
         }
         
         die();
       
    }
    
    public function queryBuilderAction(){
         $em=$this->getDoctrine()->getEntityManager();
         $curso_repo=$em->getRepository("AppBundle:Curso");
         
//         $query=$curso_repo->createQueryBuilder("c")
//                ->where("c.precio > :precio")
//                ->setParameter("precio", "24")
//                ->getQuery();
//                
//         $cursos=$query->getResult();
         
         $cursos=$curso_repo->getCursos();
         
          foreach ($cursos as $curso){
             
             echo $curso->getId()."<br>";
             echo $curso->getTitulo()."<br>";
             echo $curso->getDescripcion()."<br>";
             echo $curso->getPrecio()."<br>";
             
         }
         
         die();
    }
    
    public function formAction(Request $request){
        
           $curso=new Curso();
           $form=$this->createForm(CursoType::class,$curso);
           
           $form->handleRequest($request);
           
           if($form->isValid()){
               $status="Formulario válido";
               $data=array(
                 "titulo"=>$form->get("titulo")->getData(),
                 "descripcion"=>$form->get("descripcion")->getData(),
                 "precio"=>$form->get("precio")->getData()
               );
           } else{
               $status=null;
               $data=null;
           }
        
          return $this->render('AppBundle:pruebas:form.html.twig', array(
            'form'=>$form->createView(),
            'status'=>$status,
            'data'=>$data
        ));
        
    }
    
    public function validarEmailAction($email){
        $emailConstraint=new Assert\Email();
        
        $emailConstraint->message = "Pásame un correco correcto";
        
        $error=$this->get("validator")->validate(
                $email,
                $emailConstraint
                );
        if(count($error)==0){
            echo "<h1>Correo válido!!</h1>";
        } else {
            echo $error[0]->getMessage();
        }
        
        die();
    }
    
    
}
