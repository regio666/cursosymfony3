<?php

namespace BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name',TextType::class,["required"=>"required","attr"=>[
                    "class"=>"form-name form-control",
                    ]])
                ->add('surname',TextType::class,["required"=>"required","attr"=>[
                    "class"=>"form-surname form-control",
                    ]])
                ->add('email', EmailType::class,["required"=>"required","attr"=>[
                    "class"=>"form-email form-control",
                    ]])
                ->add('password', PasswordType::class,["required"=>"required","attr"=>[
                    "class"=>"form-password form-control",
                    ]])
                ->add('Guardar',SubmitType::class,["attr"=>[
                    "class"=>"form-submit form-control btn-primary btn-block",]])
               ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blogbundle_user';
    }


}
