<?php

namespace BlogBundle\Entity;

/**
 * Category
 *
 */
class Category
{
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var int
     */
    private $id;

    /**
     * 
     */
    protected $entry;
    
    public function __construct(){
        $this->entry=new \Doctrine\Common\Collections\ArrayCollection;
        
    }
    
    public function __toString() {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Category
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Category
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getEntries() {
        return $this->entry;
    }

    public function setEntries($entry) {
        $this->entry = $entry;
        return $this;
    }


}
