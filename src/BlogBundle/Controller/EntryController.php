<?php

namespace BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Entry;
use BlogBundle\Form\EntryType;



class EntryController extends Controller
{
    private $session;

    public function __construct(){
        $this->session=new Session();
    }

    public function addAction(Request $request){
        
        $entry=new Entry();
        $form=$this->createForm(EntryType::class,$entry);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            if($form->isValid()){
                /**
                 * Obtener el Id de el objeto categoría seleccionado
                 */
                $em=$this->getDoctrine()->getEntityManager();
                $entry_repo=$em->getRepository("BlogBundle:Entry");
                $category_repo=$em->getRepository("BlogBundle:Category");
                $category=$category_repo->find($form->get("category")->getData());
        
                $entry=new Entry();
                $entry->setTitle($form->get("title")->getData());
                $entry->setContent($form->get("content")->getData());
                $entry->setStatus($form->get("status")->getData());
                 /**
                 * Capturar el fichero del formulario y subirlo al servidor
                 */
                $file=$form['image']->getData();
                if(!empty($file) && $file!=null){
              
                    $ext=$file->guessExtension();
                    $file_name=time().".".$ext;
                    $file->move("uploads",$file_name);
                    $entry->setImage($file_name);
                } else {
                    $entry->setImage(null);
                }
                
                
                $entry->setCategory($category);
                
                $user=$this->getUser();
                $entry->setUser($user);
                
                $em->persist($entry);
                $flush=$em->flush();
                
                $entry_repo->saveEntryTags(
                        $form->get("tags")->getData(),
                        $form->get("title")->getData(),
                        $category,
                        $user
                        );
                
                if($flush==null){
                    $status="La categoría se ha creado correctamente !!";
                } else {
                    $status="Error al añadir la categoría!!";
                }
                
                
            $status="La categoría se ha creado correctamente";
        } else {
            $status="La categoría no se ha creado, porque el formulario no es válido!!";
        }
        $this->session->getFlashBag()->add("status", $status);
        return $this->redirectToRoute("blog_homepage");
        }
        
         
        return $this->render("BlogBundle:Entry:add.html.twig",array(
            "form"=>$form->createView()
        ));
        
    }
    
    public function indexAction(Request $request, $page){
        
       // var_dump($request->getSession()->get("_locale"));
       // var_dump($this->get("translator")->trans("btn_edit"));
        
           $em=$this->getDoctrine()->getEntityManager();
           $entries_repo=$em->getRepository("BlogBundle:Entry");
           $category_repo=$em->getRepository("BlogBundle:Category");
           
           //$entries=$entries_repo->findAll();
           $categories=$category_repo->findAll();
           /**
            * paginamos
            */
           $pageSize=5;
           $entries=$entries_repo->getPaginateEntries($pageSize,$page);
           $totalItems=count($entries);
           $pageCount=ceil($totalItems/$pageSize);
           
           
            return $this->render("BlogBundle:Entry:index.html.twig",array(
            "entries"=>$entries,
                "categories"=>$categories,
                "totalItems"=>$totalItems,
                "pageCount"=>$pageCount,
                "page"=>$page,
                "page_m"=>$page
        ));
        
        
    }
    
    public function deleteAction($id){
          $em=$this->getDoctrine()->getEntityManager();
          $entry_repo=$em->getRepository("BlogBundle:Entry");
          $entry=$entry_repo->find($id);
          $entry_tag_repo=$em->getRepository("BlogBundle:EntryTag");
          
          $entry_tags=$entry_tag_repo->findBy(array("entry"=>$entry));
          foreach ($entry_tags as $et){
              if(is_object($et)){
              $em->remove($et);
              $em->flush();
              }
          }
          
          if(is_object($entry)){
          $em->remove($entry);
          $em->flush();
          }
          
          return $this->redirectToRoute("blog_homepage");
          }
          

  
    public function editAction($id,Request $request){
        $em=$this->getDoctrine()->getEntityManager();
        $entries_repo=$em->getRepository("BlogBundle:Entry");
        $category_repo=$em->getRepository("BlogBundle:Category");
        
        $entry=$entries_repo->find($id);
        $entry_image=$entry->getImage();
        
        $tags="";
        foreach ($entry->getEntryTag() as $entryTag){
            $tags.=$entryTag->getTAg()->getName().", ";
        }
        
        $form=$this->createForm(EntryType::class,$entry);
        
        $form->handleRequest($request);
        
            if($form->isSubmitted()){
               if($form->isValid()){
                   //no es necesario ya están en el request
                 /*  $entry->setTitle($form->get("title")->getData());
                   $entry->setContent($form->get("content")->getData());
                   $entry->setStatus($form->get("status")->getData());
                   */
                   
                   $file=$form['image']->getData();
                      if(!empty($file) && $file!=null){
             
                        $ext=$file->guessExtension();
                        $file_name=time().".".$ext;
                        $file->move("uploads",$file_name);

                       $entry->setImage($file_name);
                      } else {
                          $entry->setImage($entry_image);
                      }
                   $category=$category_repo->find($form->get("category")->getData());
                   $entry->setCategory($category);
                   $user=$this->getUser();
                   $entry->setUser($user);
              
                   $em->persist($category);
                   $flush=$em->flush();
                   
                   $entry_tag_repo=$em->getRepository("BlogBundle:EntryTag");
                   $entry_tags=$entry_tag_repo->findBy(array("entry"=>$entry));
                 
                   foreach ($entry_tags as $et){
                        if(is_object($et)){
                        $em->remove($et);
                        $em->flush();
                        }
                    }
                   
                   $entries_repo->saveEntryTags(
                           $form->get("tags")->getData(),
                           $form->get("title")->getData(),
                           $category,
                           $user
                           );

                   if($flush==null){
                       $status="La entrada se ha editado correctamente !!";
                   } else {
                       $status="La entrada se ha editado incorrectamente!!";
                   }
        
           } else {
               $status="El formulario no es válido!!";
           }
           $this->session->getFlashBag()->add("status", $status);
           return $this->redirectToRoute("blog_homepage");
           }
       
             return $this->render("BlogBundle:Entry:edit.html.twig",array(
                "form"=>$form->createView(),
                "entry"=>$entry,
                "tags"=>$tags
        ));
        
          
    }
    

}
