<?php

namespace BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\Category;
use BlogBundle\Form\CategoryType;



class CategoryController extends Controller
{
    private $session;

    public function __construct(){
        $this->session=new Session();
    }

    public function addAction(Request $request){
        
        $category=new Category();
        $form=$this->createForm(CategoryType::class,$category);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            if($form->isValid()){
                
                $em=$this->getDoctrine()->getEntityManager();
                
                $category=new Category();
                $category->setName($form->get("name")->getData());
                $category->setDescription($form->get("description")->getData());
                
                $em->persist($category);
                $flush=$em->flush();
                
                if($flush==null){
                    $status="La categoría se ha creado correctamente !!";
                } else {
                    $status="Error al añadir la categoría!!";
                }
                
                
            $status="La categoría se ha creado correctamente";
        } else {
            $status="La categoría no se ha creado, porque el formulario no es válido!!";
        }
        $this->session->getFlashBag()->add("status", $status);
        return $this->redirectToRoute("blog_index_category");
        }
        
         
        return $this->render("BlogBundle:Category:add.html.twig",array(
            "form"=>$form->createView()
        ));
        
    }
    
    public function indexAction(){
        
           $em=$this->getDoctrine()->getEntityManager();
           $categories_repo=$em->getRepository("BlogBundle:Category");
           $categories=$categories_repo->findAll();
           
            return $this->render("BlogBundle:Category:index.html.twig",array(
            "categories"=>$categories
        ));
        
        
    }
    
    public function deleteAction($id){
          $em=$this->getDoctrine()->getEntityManager();
          $categories_repo=$em->getRepository("BlogBundle:Category");
          $category=$categories_repo->find($id);
          
          if(count($category->getEntries())==0){
          $em->remove($category);
          $em->flush();
          }
          
          return $this->redirectToRoute("blog_index_category");
         
    }
    
    public function editAction($id,Request $request){
        $em=$this->getDoctrine()->getEntityManager();
        $categories_repo=$em->getRepository("BlogBundle:Category");
        $category=$categories_repo->find($id);
        
        $form=$this->createForm(CategoryType::class,$category);
        
        $form->handleRequest($request);
        
            if($form->isSubmitted()){
               if($form->isValid()){

                   $em=$this->getDoctrine()->getEntityManager();

                   $category->setName($form->get("name")->getData());
                   $category->setDescription($form->get("description")->getData());

                   $em->persist($category);
                   $flush=$em->flush();

                   if($flush==null){
                       $status="La categoría se ha editado correctamente !!";
                   } else {
                       $status="Error al editar la categoría!!";
                   }


               $status="La categoría se ha editado correctamente";
           } else {
               $status="La categoría no se ha editado, porque el formulario no es válido!!";
           }
           $this->session->getFlashBag()->add("status", $status);
           return $this->redirectToRoute("blog_index_category");
           }
       
             return $this->render("BlogBundle:Category:edit.html.twig",array(
            "form"=>$form->createView()
        ));
        
          
    }
    
    public function categoryAction($id,$page){
        
        $em=$this->getDoctrine()->getEntityManager();
        $categories_repo=$em->getRepository("BlogBundle:Category");
        $category=$categories_repo->find($id);
        $categories=$categories_repo->findAll();
        
        $entry_repo=$em->getRepository("BlogBundle:Entry");
        $entries=$entry_repo->getCategoryEntries($category,3,$page);
        
         $totalItems=count($entries);
         $pageCount=ceil($totalItems/3);
      
        return $this->render("BlogBundle:Category:category.html.twig",array(
            "category"=>$category,
            "categories"=>$categories,
            "entries"=>$entries,
            "totalItems"=>$totalItems,
            "page"=>$page,
            "page_m"=>$page,
            "pageCount"=>$pageCount
                ));
    }
    

}
